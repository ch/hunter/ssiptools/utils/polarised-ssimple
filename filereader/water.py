from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface

import numpy as np
import math
global R, C, Kvdw, cmax, cc, c0
R = 0.008314
C = 55.35
cmax = 300
Kvdw = 0.5*math.exp(5.6/(R*298))


Ssip = SsipReader('surface.xml')
Ssiplist = Ssip.list_ssips







#generate input for parameters alpha and beta





def eq_constant (x, y):
    if x*y < 0:
        K = 0.5*math.exp((-x*y +5.6)/(R*298))
    else:
        K = Kvdw
    
    return K

        




#code for calculation of sovation energy for water

#function for calculation of phi_b

    
def psi_f_vdw_calculator (tetha):
    return (math.sqrt(1+8*Kvdw*tetha) -1)/(4*Kvdw*tetha)

def psi_f_calculator (x, y):
    return (x/y)



        
        


#function to calculate number of SSIPs in molecule

def numberSSIP ():
    N = int(37.7/9.35)
    return N





#function to calculate equilibrium constant



#introduce array for concentration of species
N = numberSSIP ()
i_total = C/cmax
i_free = C/cmax
j_free = C/cmax
j_total = C/cmax


ssip_free = []








for i in len(Ssiplist):
        ij_total = 0
        j=0
        
    

        K = eq_constant(Ssiplist[i], Ssiplist[j])

        
        
        while True:




            for j in len(Ssiplist):

                K = eq_constant(Ssiplist[i], Ssiplist[j])
                
                ij = K*i_free*j_free

                ij_total += ij

                j += 1


  

            i_total_apparent = 2*ij_total + i_free
            

            i_free = i_free/(math.sqrt(i_total_apparent/i_total))
            
    


            if abs((i_total_apparent-i_total)/i_total) < pow(10, -14):
                break
        
        i+= 1
        ssip_free.append(i_free)
    

i_free_total = 0

for conc in ssip_free:
    i_free_total += conc


m = psi_f_vdw_calculator(2*(i_total+j_total)/cmax)
n = psi_f_calculator(i_free_total, i_total)

print (n) 


    

 


    


    



    



