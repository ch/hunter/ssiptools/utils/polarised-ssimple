import unittest
from filereader.cml_reader import CmlReader
from filereader.mol2reader import Mol2Reader
import os
from lxml import etree
FIXTURE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_files', '')

cml_ethanol = f"{FIXTURE_DIR}/ethanolnonamespace.cml"
mol2_ethanol = f"{FIXTURE_DIR}/ethanol.mol2"
cml_sybyl = f"{FIXTURE_DIR}/ethanolnonamespacewithsybyl.cml"

class TestCmlMol2Comparison(unittest.TestCase):
    def setUp(self):
        self.cml = CmlReader(cml_ethanol, ns=None)
        self.mol2 = Mol2Reader(mol2_ethanol, from_file=True)
        self.cml_sybyl = CmlReader(cml_sybyl, ns=None)

    def tearDown(self):
        del self.cml
        del self.mol2
        del self.cml_sybyl

    def test_list_atoms_length(self):
        atom0 = self.cml.list_atoms[0]
        atom1 = self.mol2.list_atoms[0]
        self.assertTrue(self.cml._compare_atoms_(atom0, atom1))

    def test_sybyl_atoms(self):
        sybyl_dict = self.cml._get_sybyl_dict_(self.mol2)
        atom_type_0 = sybyl_dict["a1"]
        self.assertEqual(len(sybyl_dict.keys()), 9)
        self.assertEqual(atom_type_0, "C.3")

    def test_new_cml(self):
        tree_new_sybyl = self.cml.get_cml_with_sybyl_info(self.mol2)
        tree_sybyl = self.cml_sybyl.tree
        self.assertEqual(
                etree.tostring(tree_sybyl), 
                etree.tostring(tree_new_sybyl)
                )
   
if __name__ == '__main__':
    unittest.main()
