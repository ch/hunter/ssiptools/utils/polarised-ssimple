from speciation import speciation_calculator, eq_constant
from input import input_molecule
from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface
from reader import Ssip_dict, concentrations_molecules, molecules, Ssip_list, Ssip_dict
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from solvation_energy import solvation_energy_calculator



#define y axis for graph
y_points = []

#define x axis for graph
xpoints= np.linspace(input_molecule[1], input_molecule[2], input_molecule[3])

#calculate solvation energy for each solute SSIP
for i in xpoints:
    y = solvation_energy_calculator(i)
    y_points.append(y)

ypoints = np.array(y_points)

#create data frame for plot
plot_data = pd.DataFrame({'xaxis': xpoints,
                          'yaxis': ypoints})


#export plot data to analyse in excel
file_name = 'plotdata_ethanol_151_tensor_confinement_fullnewcheck_driver.xlsx'
plot_data.to_excel(file_name)


plt.plot(xpoints, ypoints, color = 'red')
plt.show()

