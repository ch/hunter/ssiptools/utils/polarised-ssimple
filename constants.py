#define constants 
import math


R = 0.008314

cmax = 300
Evdw = 5.6
Kvdw = 0.5*math.exp(Evdw/(R*298))
