import numpy as np
'''
define dictionary with all molecules that make up the solvent
and their fractions in the solvent mixture
'''
input_list = {




'ethanol': 1


}
'''
define list for solute; first value is the concentration; second and third are the range of solute SSIP; 
last is the number of data points for the solvation energy graph
'''
input_molecule = [0.001, -10, 5, 152]

'''
kappa_matrix defining cooperative interactions between hydrogen bonds; every row and column indicate interactions
corresponding to that SSIP 
'''

kappa_matrix = np.array([[0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0],
                        [0,0,0,0,0,0,0,0,0]])
                  

                  
