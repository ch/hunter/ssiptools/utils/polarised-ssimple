#import functions and libraries
from input import input_list
import csv
import pandas as pd
from filereader.ssip_reader import SsipReader

#define dictionaries
inchikey_solvents = {}    
Concentrations = {}

#create dictionary that matches solvent inchikey to molecule name
with open('solventinchikeys.csv', mode = 'r') as infile:
    
    reader = csv.reader(infile, delimiter = "\t")
    next(reader, None)



   
    for rows in reader:
        inchikey_solvents[rows[0]] = rows[1]
        

#create dictionary that matches molecule name with concentration
with open('concent.csv', mode = 'r', encoding = 'utf-8-sig') as infile:
    
    reader = csv.reader(infile,  delimiter = ",")
   

    

   
    for rows in reader:
        Concentrations[rows[0]] = float(rows[1])       
        
     



molecules =[]

concentrations_molecules = []



#create list of molecuiles in mixture
for Molecule in input_list.keys():
    molecules.append(Molecule)





Ssip_list = []



Ssip_dict = {}


#create dictionary that matches moelcule name with its SSIP list
def get_ssips():
    Ssip_dict = {}
    for molecule in molecules:
        inchi = inchikey_solvents[molecule]
        ssip_molecule = SsipReader(f'unpolarisedssipfiles/{inchi}_ssip.xml')
        Ssiplist_molecule = [i.value for i in ssip_molecule.list_ssips]
        Ssip_dict[molecule] = Ssiplist_molecule
    return Ssip_dict

Ssip_dict = get_ssips()


#create lists of SSIPs and cocnentrations of molecules
for molecule in molecules:

    Ssip_list += Ssip_dict[molecule]

    ratio = input_list[molecule]
    solvent_conc = Concentrations[molecule]
    
    conc = ratio *solvent_conc
    concentrations_molecules.append(conc)








