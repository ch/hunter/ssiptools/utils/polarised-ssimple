#import functions and libraries
from speciation import speciation_calculator, eq_constant 
from input import input_molecule
from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface
from reader import Ssip_list
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from solvation_energy import get_population

#define the three population graphs correspondign to solute binding to CH, OH and O
y1_points = []
y2_points = []
y3_points = []

#define x axis elements
xpoints= np.linspace(input_molecule[1], input_molecule[2], input_molecule[3])

#get solvation energies for all graphs
N = len(Ssip_list)
for i in xpoints:
    y1, y2, y3, ij_matrix = get_population(i, N)
    y1_points.append(y1*100/(y1+y2+y3))
    y2_points.append(y2*100/(y1+y2+y3))
    y3_points.append(y3*100/(y1+y2+y3))


y1points = np.array(y1_points)
y2points = np.array(y2_points)
y3points = np.array(y3_points)

#create data frame for plot
plot_data = pd.DataFrame({'xaxis': xpoints,
                          'y1axis': y1points,
                          'y2axis': y2points,
                          'y3axis': y3points})


#export plot data to analyse in excel
file_name = 'population_ethanol_151_tensor_confinement_fullnewcheck_driver.xlsx'
plot_data.to_excel(file_name)

#make plot
plt.plot(xpoints, y1points, color = 'grey', label = 'C-H')
plt.plot(xpoints, y2points, color = 'red', label = 'O-H')
plt.plot(xpoints, y3points, color = 'blue', label = 'O')
plt.legend(loc='upper left')
plt.show()
