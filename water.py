from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface
from constants import R, cmax, Kvdw
from input import input_molecule
from reader import Ssip_list_polarized_groups, Ssip_dict_polarized_groups
import math
import numpy as np
#generate input for parameters alpha and beta; keep track of all ij in a matrix; output all of them; interactions of everything with everything
def eq_constant (x, y):
    if x*y < 0:
        K = 0.5*math.exp((-x*y +5.6)/(R*298))
    else:
        K = Kvdw
    return K

def kappa_constant (a,b,k):
    K_kappa = math.exp(-a*b*k/(R*298))
    return K_kappa

def get_list_polarized(j, Ssip_dict_polarized_groups):
    for v in Ssip_dict_polarized_groups.values():
        if j in v:
            return v

def get_indices(list_group, ssip_list):
    k = []
    for v in list_group:
        w = ssip_list.index(v)
        k.append(w)
    return k

#code for calculation of sovation energy for water
#introduce array for concentration of species

def speciation_calculator (Ssiplist, concentrations_molecules, Ssip_dictionary, Molecule, ssip):
    Ssiplist1 = Ssiplist.copy()
    Ssiplist2 = Ssiplist.copy()
 
    ssip_free = []
    ssip_total = []
    ssip_total_apparent = []
    List_group = []
    l=0
    if len(Molecule) == 1:
        for i in Ssiplist1:
            C= concentrations_molecules[0]/cmax
            ssip_free.append(C)
        C = input_molecule[0]/cmax
        ssip_free.append(C)
    else:
        for i in Ssiplist1:
        
            if i in Ssip_dictionary[Molecule[l]]:
                C = concentrations_molecules[l]/cmax
            else:
                l +=1
                C = concentrations_molecules[l]/cmax
            ssip_free.append(C)
        C = input_molecule[0]/cmax
        ssip_free.append(C)
        
    ssip_total = ssip_free.copy()
    N = len(ssip_free)
    for i in range(0,N):
        ssip_total_apparent.append(0)
    
    Ssiplist1.append(ssip)
    Ssiplist2.append(ssip)

    while True:
        for t,i in enumerate(Ssiplist1):
            ij_total = 0
            for m,j in enumerate(Ssiplist2):
                K_ij = eq_constant(i, j)

                if j in Ssip_list_polarized_groups and j != Ssiplist2[N-1]:
                    List_group = get_list_polarized(j, Ssip_dict_polarized_groups)
                    sum_rk_lower = 0
                    sum_rk_upper = 0
                    sum_rksl_upper = 0
                    sum_rksl_lower = 0
                    indices_list=[]
                    indices_list = get_indices(List_group, Ssiplist1)
                    for r in indices_list:
                        if r == m:
                            continue
                        else:
                            for s in indices_list:
                                if s==m or s==r:
                                    continue
                                else:
                                    for k in range(0, len(Ssiplist1)):
                                        K_rk = eq_constant(Ssiplist1[r], Ssiplist1[k])
                                
                                        conc_k = ssip_free[k]
                                        rk = K_rk*conc_k 
                                        ab = rk #this is not a copy
                                        sum_rk_lower += rk
                                        if Ssiplist1[r]*j >0:
                                            rk = kappa_constant(i, Ssiplist1[k], 0)*rk
                                        else:
                                            rk = kappa_constant(i, Ssiplist1[k], 0.33)*rk
                                        sum_rk_upper += rk
                                        for l in range(0, len(Ssiplist1)):
                                            
                                            K_sl = eq_constant(Ssiplist1[s], Ssiplist1[l])
                                            conc_l = ssip_free[l]
                                            sl= K_sl*conc_l
                                            cd =sl #this is not a copy
                                            if Ssiplist1[s]*j >0:
                                                sl = kappa_constant(i, Ssiplist1[l], 0)*sl
                                            else:
                                                sl = kappa_constant(i, Ssiplist1[l], 0.33)*sl
                                            rksl_upper = rk*sl
                                            if Ssiplist1[r]*Ssiplist1[s] >0:
                                                rksl_upper = kappa_constant(Ssiplist1[k], Ssiplist1[l], 0)*rksl_upper
                                                rksl_lower = kappa_constant(Ssiplist1[k], Ssiplist1[l], 0)*ab*cd
                                            else:
                                                rksl_upper= kappa_constant(Ssiplist1[k], Ssiplist1[l], 0.33)*rksl_upper
                                                rksl_lower = kappa_constant(Ssiplist1[k], Ssiplist1[l], 0.33)*ab*cd
                                            sum_rksl_upper += rksl_upper
                                            sum_rksl_lower += rksl_lower

                    polarisation_ratio = (1+0.5*sum_rk_upper+0.5*sum_rksl_upper)/(1+0.5*sum_rk_lower+0.5*sum_rksl_lower)
                    ij = K_ij*ssip_free[t]*ssip_free[m]*polarisation_ratio 
                else:
                    ij = K_ij*ssip_free[t]*ssip_free[m]
                if i ==j:
                    ij_total += ij
                else:
                    ij_total += 2*ij
            i_total_apparent = ij_total + ssip_free[t]
            ssip_total_apparent[t] = i_total_apparent
            
        for i in range(0,N):
            ssip_free[i] = ssip_free[i]/(np.power((ssip_total_apparent[i]/ssip_total[i]), 0.25))
        p=0
        for i in range(0,N):
            if abs((ssip_total_apparent[i]-ssip_total[i])/ssip_total_apparent[i]) < pow(10, -6):
                p +=1
        if p==N:
            break
        
    return ssip_free, ssip_total_apparent        
            
"""""
                if j in Ssip_list_polarized_groups and j != Ssiplist2[N-1]:
                    List_group = get_list_polarized(j, Ssip_dict_polarized_groups)
                    sum_rk_lower = 0
                    sum_rk_upper = 0
                    sum_rksl_upper = 0
                    sum_rksl_lower = 0
                    indices_list=[]
                    indices_list = get_indices(List_group, Ssiplist1)
                    for r in indices_list:
                        if r == m:
                            continue
                        else:
                            for s in indices_list:
                                if s==m or s==r:
                                    continue
                                else:
                                    for k in range(0, len(Ssiplist1)):
                                        
                                        K_rk = eq_constant(Ssiplist1[r], Ssiplist1[k])
                                        conc_k = ssip_free[k]
                                        rk = K_rk*conc_k
                                        ab = rk
                                        sum_rk_lower += rk
                                        if Ssiplist1[r]*j >0:
                                            rk = kappa_constant(i, Ssiplist1[k], 0)*rk
                                        else:
                                            rk = kappa_constant(i, Ssiplist1[k], 0)*rk
                                        sum_rk_upper += rk
                                        for l in range(0, len(Ssiplist1)):
                                            
                                            K_sl = eq_constant(Ssiplist1[s], Ssiplist1[l])
                                            conc_l = ssip_free[l]
                                            sl= K_sl*conc_l
                                            cd =sl
                                            if Ssiplist1[s]*j >0:
                                                sl = kappa_constant(i, Ssiplist[l], 0)*sl
                                            else:
                                                sl = kappa_constant(i, Ssiplist[l], 0)*sl
                                            rksl_upper = rk*sl
                                            if Ssiplist1[r]*Ssiplist1[s] >0:
                                                rksl_upper = kappa_constant(Ssiplist[k], Ssiplist[l], 0)*rksl_upper
                                                rksl_lower = kappa_constant(Ssiplist[k], Ssiplist[l], 0)*ab*cd
                                            else:
                                                rksl_upper= kappa_constant(Ssiplist[k], Ssiplist[l], 0)*rksl_upper
                                                rksl_lower = kappa_constant(Ssiplist[k], Ssiplist[l], 0)*ab*cd
                                            sum_rksl_upper += rksl_upper
                                            sum_rksl_lower += rksl_lower

                    polarisation_ratio = (1+0.5*sum_rk_upper+0.5*sum_rksl_upper)/(1+0.5*sum_rk_lower+0.5*sum_rksl_lower)
                    ij = K_ij*ssip_free[t]*ssip_free[m]*polarisation_ratio 
                else:
                    ij = K_ij*ssip_free[t]*ssip_free[m]
"""""








    

 


    


    



    



