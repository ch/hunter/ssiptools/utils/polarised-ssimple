#import function and libraries necessary

from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface
from constants import R, cmax, Kvdw
from input import input_molecule, kappa_matrix
from itertools import combinations
import math
import torch
import numpy as np
import copy

#define function to calculate equilibrium constant of pairwise interaction of SSIPs

def eq_constant (x, y):
    if x*y < 0:
        K = 0.5*math.exp((-x*y +5.6)/(R*298))
    else:
        K = Kvdw
    
    return K

'''
Define function to calculate the K_matrix; each cell contains the equilibrium constant corresponding 
to the interaction of the two SSIPs on teh row and column respectively; equilibrium constant calculated
using the eq_constant function above    

'''
def K_matrix_generator(Ssiplist, Ssiplist1):
    N = len(Ssiplist1)
    M = len(Ssiplist)
    K_matrix = np.zeros((M,N))

    for i,m in enumerate(Ssiplist):
        for j,n in enumerate(Ssiplist1):

            K_matrix[i][j] = eq_constant(m, n)
    return K_matrix

'''
Define function get_P_tensor to calculate tensor P, which contains elements P_ijkl, the cooperativity
constant for interaction between hydrogen bonds i-j and k-l; tensor P has 4 dimensions,
corresponding to the 4 different SSIPs interacting; 

'''

def get_P_tensor(Ssiplist, Ssiplist1, kappa_matrix):

    #dimension for all SSIPs
    N = len(Ssiplist1)

    #dimension for SSIPs inside polarised molecule
    M = len(Ssiplist)


    Ssip_list1 = np.array(Ssiplist1)
    Ssip_list = np.array(Ssiplist)

    
    intermediary1 = np.multiply.outer(kappa_matrix, Ssip_list1)

    #calculate all combinations of 𝜖𝑖*𝜅*𝜖𝑙, for all 2-site cooperative complexes
    intermediary2 = np.multiply.outer(Ssip_list1, intermediary1)

    #indices array to index tensor P
    indices_P = np.indices([N, M, M, N])

    #filter matrix to correct sign errors
    Filter = (Ssip_list1[indices_P[0]] * Ssip_list[indices_P[1]] < 0) & (Ssip_list[indices_P[2]]*Ssip_list1[indices_P[3]] < 0)
    s = Filter.shape
    intermediary3 = np.multiply(intermediary2, Filter)
    
    #correcting diagonal elements by making them all zero
    intermediary3[:, range(M), range(M), :] = pow(10,8)
   

    reverse_intermediary = intermediary3*(-1)

    preliminary = reverse_intermediary * (1/(R*298))
    

    P = np.exp(preliminary)
    

    return P

'''
Define function ijkl_combinations to get combinations of four indices corresponding to all possible 
cooperative interactions between two hydrogen bonds 

'''

def ijkl_combinations(N):
    List = list(range(N))
    new_list = []

    #get pairs of SSIPs
    for i in range(0,N,2):
        
        new_list.append([List[i], List[i+1]])

    #get combinations of 2 hydrogen bonds (4 SSIPs) interacting
    combination_list = torch.tensor(list(combinations(new_list, 2)))
    flattened_combinations_list = torch.flatten(combination_list, start_dim = 1)

    #ensure correct order in the lists of 4 SSIPs
    for rows in flattened_combinations_list:
        if rows[0] != 0:
            a = copy.deepcopy(rows[0])
            rows[0] = rows[1]
            rows[1] = a


    return flattened_combinations_list

'''
Define function lkmn_combinations to get combinations of four indices corresponding to all possible 
cooperative interactions between two hydrogen bonds apart from i-j 

'''


def lkmn_combinations(N):
    List = list(range(N))
    new_list = []

    #get pairs of SSIPs
    for i in range(2,N,2):
        
        new_list.append([List[i], List[i+1]])

    #get combinations of 2 hydrogen bonds (4 SSIPs) interacting
    combination_list = torch.tensor(list(combinations(new_list, 2)))
    flattened_combinations_list = torch.flatten(combination_list, start_dim = 1)

    #ensure correct order in the list of 4 SSIPs
    for rows in flattened_combinations_list:
        if rows[0] != 0:
            a = copy.deepcopy(rows[0])
            rows[0] = rows[1]
            rows[1] = a



    return flattened_combinations_list  

'''
Define function speciation_calculator to output concentrations of free and total SSIP, as well as matrix
showing concentrations of all pairwise complexes, ij_matrix; this is the core function of the algorithm,
containing the COGS optimisation loop

'''

def speciation_calculator (Ssiplist, concentrations_molecules, Ssip_dictionary, Molecule, ssip):

    #define necessary arrays
    Ssiplist1 = Ssiplist.copy()
    Ssiplist2 = Ssiplist.copy()
    

    ssip_free = []
    ssip_total = []
    ssip_total_apparent = []
    

    l=0



    #start with the guess concentration of C_liquid (concentration of pure liquid) for all SSIPs
    if len(Molecule) == 1:
        for i in Ssiplist1:
            C= concentrations_molecules[0]/cmax
            ssip_free.append(C)
        C = input_molecule[0]/cmax
        ssip_free.append(C)
    else:


        for i in Ssiplist1:
        
            if i in Ssip_dictionary[Molecule[l]]:
                C = concentrations_molecules[l]/cmax
            else:
                l +=1
                C = concentrations_molecules[l]/cmax
            ssip_free.append(C)
        C = input_molecule[0]/cmax
        ssip_free.append(C)
        
    ssip_total = ssip_free.copy()
    N = len(ssip_free)
    M = len(Ssiplist)
    
    #ssip_total_apparent is initialised as zero for all SSIPs
    for i in range(0,N):
        ssip_total_apparent.append(0)
    
    #append the solute SSIP to the lists
    Ssiplist1.append(ssip)
    Ssiplist2.append(ssip)

    #initialise interactions matrix
    ij_matrix = np.zeros((N,N))

    K_matrix = K_matrix_generator(Ssiplist, Ssiplist1)

    P_tensor = get_P_tensor(Ssiplist, Ssiplist1, kappa_matrix)
    
    #get the order of cooperation from the rank of the kappa_matrix
    maximum_order = np.linalg.matrix_rank(kappa_matrix)

    #COGS optimisation loop
    while True:

        #vector of free concentrations for all SSIPs
        l_vector = np.array(ssip_free) 
        '''
        initialise NxM matrix with each element representing sum over all cooperative complexes
        for fixed i and j; upper label indicates sums are for numerator of polarisation ratio  
        '''
        j_upper = np.full((N,M), 1, dtype = float)

        '''
        initialise NxM matrix with each element representing sum over all cooperative complexes
        for fixed i and j; lower label indicates sums are for denominator of polarisation ratio  
        '''
        j_lower = np.full((N,M), 1, dtype = float)

        #calculate j_upper and j_lower    
        for n in range(2, maximum_order+1):

            Kl = K_matrix * l_vector
            
            #calculate B tensor
            B = Kl
            for _ in range(0, n-2):
                B = np.multiply.outer(B, Kl) 
                            
            
            if n==2:
                output_lower = np.array([[np.sum(np.delete(Kl.T, column,1)) for column in range(M)] for row in range(N)])

            else:

                #indices array to index all combinations of SSIPs involved in cooperative complex of order n
                indices_lower = np.indices([N,M] + [M,N]*(n-1))
                
                #tensor with elements as products of cooperativity constants P_lkmn according to the order n 
                A_lower = np.prod(P_tensor[*indices_lower[np.array(lkmn_combinations(2*n)).T]], axis=0)

                #sums over cooperative constats for order n               
                output_lower = np.tensordot(A_lower, B, axes = 2*(n-1))
                
            
            
            #indices array to index all combinations of SSIPs involved in cooperative complex of order n
            indices_upper = np.indices([N,M] + [M,N]*(n-1))

            #tensor with elements as products of cooperativity constants P_ijkl according to the order n
            A_upper = np.prod(P_tensor[*indices_upper[np.array(ijkl_combinations(2*n)).T]], axis=0)
          
            #sums over cooperative constats for order n
            output_upper = np.tensordot(A_upper, B, axes = 2*(n-1))
            
            j_upper += output_upper
            j_lower += output_lower  
        
        #iteration over all i 
        for t,i in enumerate(Ssiplist1):

            #reinitialise ij complex at beginning of each iteration
            ij_total = 0

            #iteration over all j
            for m,j in enumerate(Ssiplist2):
                K_ij = eq_constant(i, j)

                #exclude solute SSIP, which is not polarised 
                if m != (N-1):

                    #polarisation ratio for given i and j
                    ratio = j_upper[t][m]/j_lower[t][m]
                    ij = K_ij*ssip_free[t]*ssip_free[m]*ratio
                else:

                    ij = K_ij*ssip_free[t]*ssip_free[m]               

                    
                ij_matrix[t][m] = ij

                if t == m:
                    ij_total += ij
                else:
                    ij_total += 2*ij
                

            i_total_apparent = ij_total + ssip_free[t]
            ssip_total_apparent[t] = i_total_apparent
            
        #adjusting concentration of free ssip each round
        for i in range(0,N):

            ssip_free[i] = ssip_free[i]/(np.power((ssip_total_apparent[i]/ssip_total[i]), 1/(maximum_order+1)))
            
        
        #convergence criterion
        p=0
        for i in range(0,N):
            if abs((ssip_total_apparent[i]-ssip_total[i])/ssip_total_apparent[i]) < pow(10, -6):
                p +=1
        if p==N:
            break
        

    return ssip_free, ssip_total_apparent, ij_matrix        
            


        
            
   
        
    



 


    


    



    



