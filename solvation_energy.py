#import libraries and functions
from speciation import speciation_calculator, eq_constant
from filereader.cml_reader import CmlReader, CML_NS
from filereader.ssip_reader import SsipReader, Ssip, SsipSurface
from reader import Ssip_dict, concentrations_molecules, molecules, Ssip_list, Ssip_dict
import math
import numpy as np
from constants import R


Ssip_fraction = []
Ssip_free = []
Ssip_total = []


#function defined to calculate solvation energy for each solute SSIP
def solvation_energy_calculator(ssip):
    N = len(Ssip_list)
    ij_matrix = np.zeros((N+1,N+1))

    Ssip_free, Ssip_total, ij_matrix = speciation_calculator(Ssip_list, concentrations_molecules, Ssip_dict, molecules, ssip)
           
       

    Ssipfree = np.array(Ssip_free)
    Ssiptotal = np.array(Ssip_total)
    Ssipfraction = Ssipfree/Ssiptotal

    tetha = sum(Ssiptotal)
     
    Ssipfractions = Ssipfraction.tolist()
    
    f = Ssipfractions[N]

    binding_energy = R*298*math.log(f)
    confinement_energy = -R*298*math.log((np.power(1+8*tetha, 0.5)-1)/(4*tetha))
    solvation_energy = binding_energy + confinement_energy
    return solvation_energy


#function defined to calculate population for each solute SSIP
def get_population(ssip, N):
    sum_OH = 0
    sum_CH = 0
    sum_O = 0
    ij_matrix = np.zeros((N+1,N+1))
    
    Ssip_free, Ssip_total, ij_matrix = speciation_calculator(Ssip_list, concentrations_molecules, Ssip_dict, molecules, ssip)
           
       

    for i in range(N):
        if Ssip_list[i]>2.5:
            sum_OH += ij_matrix[i][N]
        elif Ssip_list[i]>-0.5 and Ssip_list[i]<1.5:
            sum_CH += ij_matrix[i][N]
        elif Ssip_list[i]< -1.5:
            sum_O += ij_matrix[i][N]

    return sum_CH, sum_OH, sum_O, ij_matrix








