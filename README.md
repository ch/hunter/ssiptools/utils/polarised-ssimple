1) This is an extension of the original SSIMPLE code, used to calculate solvation energies for a polarised solvent


2) INSTALLATION

Program needs the following libraries installed:

filereader (internal library, found in SSIMPLE/utils), mendeleev, torch, typing-extensions, pandas, matplotlib

3) Input 

The input_list is a dictionary that contains the molecules in the mixture and their molar ratio respectively. 

Input_molecule specifies the concentration of the desired molecule, the lowest and highest value of the 'dummy' ssip and the number of points for the solvation graph respectively. The interaction of the 'dummy' ssip is calculated with the liquid so that solvation graphs are plotted.  

The kappa_matrix shows the cooperative polarization interctions inside the desired molecule


4) Running

Each program is designed to output a specific result. When one result is needed, run the specific file.